#!/bin/bash

set -e

exec 3>&1
export LANG=C
pretty() { echo -e "\x1b[32m\x1b[1m[+] ${1:+NS$1: }${2}\x1b[0m" >&3; }
pp() { pretty "" "$*"; "$@"; }
slp() { read -t "$1" -N 1 || true; }

do_cleanup() {
	set +e
	exec 2>/dev/null
	pp systemctl stop mptcpd
	pp ip link del dev mptcp-dummy0 || true
# note 0: endpoints are still present after interface removal
	pp ip mptcp endpoint flush
	exit
}

trap do_cleanup EXIT

do_setup() {
	ip mptcp endpoint flush || true
	ip link del dev mptcp-dummy0  2>/dev/null || true
	pp modprobe dummy
	pp systemctl start mptcp
	pp pidof mptcpd
	pp ip mptcp limits set add_addr_accepted 8 subflows 8
	pp ip link add name mptcp-dummy0 type dummy
#	sysctl -w net.ipv6.conf.mptcp-dummy0.disable_ipv6=1
	pp ip link set dev mptcp-dummy0 up
	pp ip address add dev mptcp-dummy0 192.0.2.10/24

}

do_check0() {
	pp slp 1
#	pp ip mptcp endpoint show | grep "fe80.*dev mptcp-dummy0"
	pp ip mptcp endpoint show | grep "192\.0\.2\.10.*dev mptcp-dummy0"
}

do_setup
do_check0
#do_cleanup
